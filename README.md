# Удобная читалка комиксов с кэшированием с сайта "авторский комикс".

### Приложение в Google Play
https://play.google.com/store/apps/details?id=newbilius.com.online_comics_reader

### Сайт "Авторский комикс"
https://acomics.ru/

### Скриншоты

![Скриншот](readme_pics/pic1.png)

![Скриншот](readme_pics/pic3.png)
